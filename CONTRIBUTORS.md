# Contributors

- Marc Levesque <Qormix@gmail.com> (https://marc.levesque.me) - Provided most of the library changes
- Cam Maccoll <cannaccoll@gmail.com> (https://github.com/maccam) - Provided the name.
