package ca.qormix.tgtos.motors;

public enum NeutralMode {
	EEPROMSetting(0),
	Coast(1),
	Brake(2);

	public int val;
	NeutralMode(int val)
	{
		this.val = val;
	}
};