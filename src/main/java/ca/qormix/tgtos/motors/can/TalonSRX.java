package ca.qormix.tgtos.motors.can;

import edu.wpi.first.wpilibj.hal.FRCNetComm;
import edu.wpi.first.wpilibj.hal.HAL;

public class TalonSRX extends MotorBase {

	public TalonSRX(int deviceNumber) {
		super(deviceNumber | 0x02040000);
		HAL.report(FRCNetComm.tResourceType.kResourceType_CANTalonSRX, deviceNumber + 1);
	}
}
