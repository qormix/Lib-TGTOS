package ca.qormix.tgtos.motors.can;

import ca.qormix.tgtos.FeedbackDevice;
import ca.qormix.tgtos.motors.ControlMode;
import ca.qormix.tgtos.motors.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.MotControllerJNI;

import static ca.qormix.tgtos.motors.ControlMode.*;
import static com.ctre.phoenix.motorcontrol.can.MotControllerJNI.*;

public class MotorBase {

	private ControlMode mode = Percent;
	private final long handle;

	public final int id;



	MotorBase(int id) {

		this.id = id;
		handle = Create(id);
	}

	public void set(ControlMode mode) { this.mode = mode; }

	public void set(NeutralMode mode) { MotControllerJNI.SetNeutralMode(handle, mode.val); }

	public void set(FeedbackDevice.Sensor sensor) { }

	public void set(double output) { set(output, 0); }

	private void set(MotorBase follow) {

		int id32 = follow.getDeviceID();
		int id24 = id32;
		id24 >>= 16;
		id24 = (short) id24;
		id24 <<= 8;
		id24 |= (id32 & 0xFF);
		mode = Follower;
		set(id24,0);
	}

	private ControlMode getControlMode() { return mode; }

	private FeedbackDevice getFeedBackDevice() { return null; }

	private void set(double... demands) {

		switch (mode) {

			default: SetDemand(handle, mode.val, 0, 0);

			case Percent: SetDemand(handle, mode.val, (int) (1023 * demands[0]), 0); break;
			case MotionProfile: SetDemand(handle, mode.val, (int) demands[0], 0); break;
			case Current: SetDemand(handle, mode.val, (int) (1000 * demands[0]), 0); break;

			case Follower:

				int demand;

				if ((0 <= demands[0]) && (demands[0] <= 62)) {

					demand = id;
					demand >>= 16;
					demand <<= 8;
					demand |= ((int) demands[0]) & 0xFF;

				} else demand = (int) demands[0];

				SetDemand(handle, mode.val, demand, 0);

				break;
		}
	}

	public int getDeviceID() { return id; }
}
