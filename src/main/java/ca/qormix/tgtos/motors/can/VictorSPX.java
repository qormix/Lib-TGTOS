package ca.qormix.tgtos.motors.can;

import edu.wpi.first.wpilibj.hal.HAL;

public class VictorSPX extends MotorBase {

	private VictorSPX(int id) {

		super(id | 0x01040000);
		HAL.report(65, id + 1);
	}
}
