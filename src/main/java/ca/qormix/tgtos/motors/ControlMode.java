package ca.qormix.tgtos.motors;

public enum ControlMode {
	Percent(0),
	Position(1),
	Velocity(2),
	Current(3),
	Follower(5),
	MotionProfile(6),
	MotionMagic(7),
	MotionMagicArc(8),
	//TimedPercentOutput(9),
	MotionProfileArc(10),

	Disabled(15);

	public final int val;

	ControlMode(int val)
	{
		this.val = val;
	}
}
